<?php

// database connection
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "demo_morning";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
// fetch data
$sql = "SELECT * FROM contact";
$result = $conn->query($sql);


$conn->close();
// show data to user

?>
<html>
    <head>
        <title>List of all contact messages</title>
        <style>
            table, th, td {
            border: 1px solid;
            }
        </style>
    </head>
    <body>

    <a href="create.php">Create new received message</a>
    
    <h2>List of all received messages</h2>

    <table>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Subject</th>
            <th>Message</th>
            <th>Action</th>
        </tr>
        <?php
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
            ?>
                <tr>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['email']; ?></td>
                    <td><?php echo $row['phone_number']; ?></td>
                    <td><?php echo $row['subject']; ?></td>
                    <td><?php echo $row['message']; ?></td>
                    <td>
                        <a href="edit.php?id=<?php echo $row['id']; ?>">Edit</a> | 
                        <a href="delete.php?id=<?php echo $row['id']; ?>">Delete</a>
                    </td>
                </tr>
            <?php
            }
        } else {
            echo "0 results";
        }
        ?>
        
    </table>

    </body>
</html>

http://localhost/demo_morning/delete.php?id=3