<?php

// database connection
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "demo_morning";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// fetch data
$id = $_GET['id'];
$sql = "SELECT * FROM contact where id = '$id'";
$result = $conn->query($sql);

while($row = $result->fetch_assoc()) {

    $id = $row['id'];
    $name = $row['name'];
    $email = $row['email'];
    $phone_number = $row['phone_number'];
    $subject = $row['subject'];
    $message = $row['message'];

}

?>

<html>
    <head>
        <title>Edit | Morning Demo Class</title>
    </head>
    <body>

    <a href="index.php">List of all messages</a>

    <br>
    <br>
        
    <form action="update.php" method="GET">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        Name
        <br>
        <input type="text" name="name" value="<?php echo $name ?>">
        <br><br>
        Email Address
        <br>
        <input type="text" name="email" value="<?php echo $email ?>">
        <br><br>
        Phone Number
        <br>
        <input type="text" name="phone_number" value="<?php echo $phone_number ?>">
        <br><br>
        Subject
        <br>
        <select name="subject">
            <option value="website" 
                <?php if($subject == 'website') echo 'selected';  ?>>
                Website
            </option>
            <option value="hosting"
                <?php if($subject == 'hosting') echo 'selected';  ?>>
                Hosting
            </option>
            <option value="domain_registration"
                <?php if($subject == 'domain_registration') echo 'selected';  ?>>
                Domain Registration
            </option>
        </select>
        <br><br>
        Message
        <br>
        <textarea name="message"><?php echo $message ?></textarea>
        <br>
        <br>
        <input type="submit" value="Update Message">
    </form>

    </body>
</html>
