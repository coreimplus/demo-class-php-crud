<?php

// database connection

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "demo_morning";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// receive data

$id = $_GET['id'];

// delete data

$sql = "DELETE from contact where id = '$id';";

if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    die();
  }

  
// redirect or send user to update list page
header('Location: index.php');